﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using vm.Application.Interfaces;

namespace vm.BackendApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GeometryController : ControllerBase
    {
        private readonly IGeometryService _pointService;
        public GeometryController(IGeometryService pointService)
        {
            _pointService = pointService;
        }
        [HttpGet]
        [Route("getall")]
        [AllowAnonymous]
        public async Task<IActionResult> GetAll(Guid? areaId)
        {
            var result = await _pointService.GetListGeometriesByArea(areaId);
            return Ok(result);
        }
    }
}
