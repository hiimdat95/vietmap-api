﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using vm.Application.Interfaces;

namespace vm.BackendApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AreaController : ControllerBase
    {
        private readonly IAreaService _areaService;
        public AreaController(IAreaService areaService)
        {
            _areaService = areaService;
        }
        [HttpGet]
        [Route("detail")]
        [AllowAnonymous]
        public async Task<IActionResult> GetDetailArea(Guid areaId)
        {
            var result = await _areaService.GetDetailArea(areaId);
            return Ok(result);
        }
    }
}
