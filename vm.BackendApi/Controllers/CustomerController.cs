﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using vm.Application.Interfaces;
using vm.Utilities.Common;

namespace vm.BackendApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomerService _customerService;

        public CustomerController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        [HttpPost]
        [Route("search")]
        [AllowAnonymous]
        public async Task<IActionResult> GetAll([FromBody] PaginatedInputModel pagingParams)
        {
            var result = await _customerService.DynamicPaging(pagingParams);
            return Ok(result);
        }
    }
}