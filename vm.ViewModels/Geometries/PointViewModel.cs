﻿using System;

namespace vm.ViewModels.Deparment
{
    public class GeometryViewModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ThumbnailImg { get; set; }
        public string VideoUrl { get; set; }
        public DateTime? DateExpire { get; set; }
        public string GeoJson { get; set; }
        public int? SortOrder { get; set; }
        public bool? Status { get; set; }
        public Guid? GeometriesParentId { get; set; }
        public Guid? MakerTypeId { get; set; }
        public Guid? GeometryTypeId { get; set; }
        public string Icon { get; set; }
    }
}