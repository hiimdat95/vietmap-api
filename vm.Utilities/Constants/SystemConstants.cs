﻿namespace vm.Utilities.Constants
{
    public class SystemConstants
    {
        public const string MainConnectionString = "Default";

        public class Claims
        {
            public const string Permissions = "permissions";
        }

        public class AppSettings
        {
            public const string DefaultLanguageId = "DefaultLanguageId";
            public const string Token = "Token";
            public const string BaseAddress = "BaseAddress";
        }

        public class Roles
        {
            public const string Admin = "Admin";
        }

        public class FileFolders
        {
            public const string Products = "products";
            public const string Attachments = "attachments";
        }
    }
}