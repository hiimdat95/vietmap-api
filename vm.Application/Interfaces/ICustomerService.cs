﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using vm.Utilities.Common;
using vm.ViewModels.Customer;

namespace vm.Application.Interfaces
{
    public interface ICustomerService
    {
        Task<PaginatedList<CustomerViewModel>> DynamicPaging(PaginatedInputModel pagingParams);
    }
}
