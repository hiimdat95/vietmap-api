﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using vm.ViewModels.Deparment;

namespace vm.Application.Interfaces
{
    public interface IGeometryService
    {
        Task<IEnumerable<GeometryViewModel>> GetListGeometriesByArea(Guid? areaId);
    }
}
