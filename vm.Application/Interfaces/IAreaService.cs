﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using vm.ViewModels.Areas;

namespace vm.Application.Interfaces
{
    public interface IAreaService
    {
        Task<AreaViewModel> GetDetailArea(Guid id);
    }
}
