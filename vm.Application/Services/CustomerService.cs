﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using vm.Application.Interfaces;
using vm.Core.EF;
using vm.Utilities.Common;
using vm.ViewModels.Customer;

namespace vm.Application.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly AppDbContext _context;
        public CustomerService(AppDbContext context)
        {
            _context = context;
        }
        public async Task<PaginatedList<CustomerViewModel>> DynamicPaging(PaginatedInputModel pagingParams)
        {
            var result = new PaginatedList<CustomerViewModel>();
            //var listItem = await _context.VIndividualCustomer.Select(x => new CustomerViewModel
            //{
            //    BusinessEntityId = x.BusinessEntityId,
            //    Title = x.Title,
            //    FirstName = x.FirstName,
            //    MiddleName = x.MiddleName,
            //    LastName = x.LastName,
            //    Suffix = x.Suffix,
            //    PhoneNumber = x.PhoneNumber,
            //    PhoneNumberType = x.PhoneNumberType,
            //    EmailAddress = x.EmailAddress,
            //    EmailPromotion = x.EmailPromotion,
            //    AddressType = x.AddressType,
            //    AddressLine1 = x.AddressLine1,
            //    AddressLine2 = x.AddressLine2,
            //    City = x.City,
            //    StateProvinceName = x.StateProvinceName,
            //    PostalCode = x.PostalCode,
            //    CountryRegionName = x.CountryRegionName,
            //    Demographics = x.Demographics,
            //}).ToListAsync();
            //#region [Filter]

            //if (pagingParams != null && pagingParams.FilterParam.Any())
            //{
            //    listItem = FilterUtility.Filter<CustomerViewModel>.FilteredData(pagingParams.FilterParam, listItem).ToList() ?? listItem;
            //}

            //#endregion [Filter]

            //#region [Sorting]

            //if (pagingParams != null && pagingParams.SortingParams.Count() > 0)
            //{
            //    listItem = SortingUtility.Sorting<CustomerViewModel>.SortData(listItem, pagingParams.SortingParams).ToList();
            //}

            //#endregion [Sorting]

            //#region [Grouping]

            //if (pagingParams != null && pagingParams.GroupingColumns != null && pagingParams.GroupingColumns.Count() > 0)
            //{
            //    listItem = SortingUtility.Sorting<CustomerViewModel>.GroupingData(listItem, pagingParams.GroupingColumns).ToList() ?? listItem;
            //}

            //#endregion [Grouping]

            //#region [Paging]

            //return new PaginatedList<CustomerViewModel>
            //{
            //    PageIndex = pagingParams.PageNumber,
            //    TotalPages = (int)Math.Ceiling(listItem.Count / (double)pagingParams.PageSize),
            //    TotalItems = listItem.Count,
            //    PageData = listItem.Skip((pagingParams.PageNumber - 1) * pagingParams.PageSize).Take(pagingParams.PageSize).ToList(),
            //    HasPreviousPage = (pagingParams.PageNumber > 1),
            //    HasNextPage = (pagingParams.PageNumber < (int)Math.Ceiling(listItem.Count / (double)pagingParams.PageSize))
            //};
            //#endregion [Paging]
            return result;
        }
    }
}
