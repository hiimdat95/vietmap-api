﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using vm.Application.Interfaces;
using vm.Core.EF;
using vm.ViewModels.Deparment;

namespace vm.Application.Services
{
    public class GeometryService : IGeometryService
    {
        private readonly AppDbContext _context;

        public GeometryService(AppDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<GeometryViewModel>> GetListGeometriesByArea(Guid? areaId)
        {
            var result = await (from g in _context.Geometries.Where(x => areaId.HasValue ? x.GeometriesParentId == areaId : 1 == 1)
                                join pt in _context.GeometryTypes on g.GeometryTypeId equals pt.Id
                                join mt in _context.MakerTypes on g.MakerTypeId equals mt.Id into mt
                                from mtype in mt.DefaultIfEmpty()
                                join i in _context.Images on mtype.ImageId equals i.Id into i
                                from img in i.DefaultIfEmpty()
                                select new GeometryViewModel
                                {
                                    Id = g.Id,
                                    Title = g.Title,
                                    Description = g.Description,
                                    DateExpire = g.DateExpire,
                                    GeoJson = g.GeoJson,
                                    SortOrder = g.SortOrder,
                                    Status = g.Status,
                                    Icon = img != null ? img.FilePath : string.Empty,
                                    GeometriesParentId = g.GeometriesParentId,
                                    GeometryTypeId = g.GeometryTypeId,
                                    MakerTypeId = g.MakerTypeId,
                                    ThumbnailImg = g.ThumbnailImg,
                                    VideoUrl = g.VideoUrl
                                }).ToListAsync();
            return result;
        }
    }
}