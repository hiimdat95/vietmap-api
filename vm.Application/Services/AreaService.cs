﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using vm.Application.Interfaces;
using vm.Core.EF;
using vm.ViewModels.Areas;

namespace vm.Application.Services
{
    public class AreaService : IAreaService
    {
        private readonly AppDbContext _context;

        public AreaService(AppDbContext context)
        {
            _context = context;
        }

        public async Task<AreaViewModel> GetDetailArea(Guid id)
        {
            var result = await _context.Areas.Where(x => x.Id == id).Select(x=>new AreaViewModel
            {
                Id = x.Id,
                Name = x.Name,
                Description = x.Description,
                DateExpire = x.DateExpire,
                Lat = x.Lat,
                Long = x.Long,
                SortOrder = x.SortOrder,
                Status = x.Status,
                CenterLong = x.CenterLong,
                CenterLat = x.CenterLat,
                MinBoundLong = x.MinBoundLong,
                MinBoundLat = x.MinBoundLat,
                MaxBoundLong = x.MaxBoundLong,
                MaxBoundLat = x.MaxBoundLat
            }).FirstOrDefaultAsync();
            return result;
        }
    }
}